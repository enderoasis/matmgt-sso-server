<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sign in') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">

</head>
<body class="text-center">
    <form method="POST" class="form-signin" action="{{ route('login') }}">
    @csrf
    <img class="mb-4" width="150" height="200" src="{{ asset('images/black_logo.jpg') }}" alt="MMS">
      <h1 class="h3 mb-3 font-weight-normal">Sign In</h1>
      @error('email')
      <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
      </span>
       @enderror
      <input type="email" class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{ old('email') }}"
                                       placeholder="Email">
                                @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
       <input type="password"
                                       class="form-control mt-3 @error('password') is-invalid @enderror"
                                       placeholder="Password" name="password">
                                @error('password')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror      <div class="checkbox mt-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block mt-3" type="submit">Sign in</button>
      <p class="mt-5 mb-3 text-muted">&copy; Material Management Software {{ now()->year }}</p>
    </form>
  </body>
