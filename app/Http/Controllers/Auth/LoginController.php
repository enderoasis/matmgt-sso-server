<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginApi(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|dns|email',
            'password' => 'required|string|min:5',
        ]);

        if (auth()->attempt(['email' => $request->email, 'password' => $request->password]))
        {
            $user = \App\Models\User::where('email',$request->email)->first();
            $token = $user->createToken('Bearer')->accessToken;
            return response()->json([
                'access_token' => $token,
                'user' => auth()->user(),
            ]);
        }
        else{
            return response()->json(['response' => 'Email and password don' . "'" . 't match.'], 401);
        }
    }

    public function getUser(){
        $user = \App\Models\User::all();
        return $user;
    }
}
