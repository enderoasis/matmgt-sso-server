<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\OrgService;

class OrganizationController extends Controller
{
    public function index()
    {
        return (new OrgService)->all();
    }

    public function store(Request $request)
    {
        return (new OrgService)
                ->request($request)
                ->create();
    }

    public function update(Request $request, $id)
    {
        return (new OrgService)
                ->request($request)
                ->update($id);
    }

    public function destroy($id)
    {
        return (new OrgService)
                ->request($request)
                ->delete($id);
    }
}
