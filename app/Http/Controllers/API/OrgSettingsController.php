<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Services\OrgSettingsService;
use App\Http\Controllers\Controller;

class OrgSettingsController extends Controller
{
    public function show(Request $request, $id)
    {
        return (new OrgSettingsService)
        ->request($request)
        ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new OrgSettingsService)
        ->request($request)
        ->update($id);
    }

}
