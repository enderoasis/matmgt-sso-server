<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Services\StateService;
use App\Http\Controllers\Controller;

class StateController extends Controller
{
    public function index(){

        return (new StateService)->all();
        
    }
}

