<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        return (new UserService)->all();
    }

    public function store(Request $request)
    {
        return (new UserService)
            ->request($request)
            ->create();
        
    }

    public function show(Request $request, $id)
    {
        return (new UserService)
        ->request($request)
        ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new UserService)
        ->request($request)
        ->update($id);
    }

    public function avatarUpload(Request $request)
    {
        return (new UserService)
        ->request($request)
        ->avatarUpload();
    }
    public function destroy($id)
    {
       
    }
}
