<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\BranchService;

class BranchController extends Controller
{
   
    public function store(Request $request)
    {
        return (new BranchService)
            ->request($request)
            ->create();
        
    }

    public function show(Request $request, $id)
    {
        return (new BranchService)
            ->request($request)
            ->one($id);
    }

    public function update(Request $request, $id)
    {
        return (new BranchService)
            ->request($request)
            ->update($id);
    }

    public function destroy($id)
    {
        return (new BranchService)
            ->request($request)
            ->delete($id);
    }
}
