<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SoftwareService;


class AccSoftwareController extends Controller
{

    public function index()
    {
        return (new SoftwareService)->all();
    }

}
