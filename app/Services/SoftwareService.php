<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\AccSoftware;

class SoftwareService 
{

    public function all(){

        $acc_softs = AccSoftware::select('id','name')->get();

        return response()->json([
            'data' =>  $acc_softs
        ]);
    }

}