<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\State;

class StateService 
{

    public function all(){

        $states = State::all();

        return response()->json([
            'data' =>  $states
        ]);
    }

}