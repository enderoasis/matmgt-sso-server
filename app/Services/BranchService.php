<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Branch;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Validator;

class BranchService 
{
    // Get organization`s branches

    public function one($id){

        $branches = Branch::where('org_id',$id)
                    ->leftJoin('states', 'states.id', '=', 'branches.state_id')
                    ->leftJoin('users', 'users.id', '=', 'branches.created_by')
                    ->select('branches.id as id',
                        'branches.name as name',
                        'branches.GSID as GSID',
                        'branches.logo_path as logo_path',
                        'branches.address as address',
                        'users.name as created_by',
                        'branches.state_id as state_id',
                        'branches.city_name as city_name',
                        'states.name as state_name',
                        'branches.postal_code as postal_code',
                        'branches.notes as notes',
                        'branches.org_id as org_id',
                        'branches.created_at as created_at',
                        'branches.updated_at as updated_at'
                    )
                    ->orderBy('created_at','DESC')
                    ->get();
        
        
        return response()->json([
            'data' => $branches
        ]);
    }

    public function create(){

        $data = $this->request->all();
        $validator = Validator::make($data, [
            'GSID' => 'required|unique:organizations|unique:branches'
        ]);
        
        if ($validator->fails()) {
            return response()->json(['success' => false, 'response' => $validator->errors()], 400);
        }

        $branch = Branch::create([
            'name' => $data['name'],
            'org_id' => $data['org_id'],
            'GSID' => $data['GSID'],
            'address' => $data['address'],
            'state_id' => $data['state_id'],
            'city_name' => $data['city_name'],
            'postal_code' => $data['postal_code'],
            'created_by' => auth()->user()->id
        ]);
        
        $file = $this->request->file('logo');
        if($file){
            $fileName = auth()->id() . '_' . time() . '_'. $file->getClientOriginalName();  
            $file->move(public_path('images/logos'), $fileName);
    
            $branch->update([
                'logo_path' => url('images/logos') . "/" . $fileName,
            ]);
        }

        return response()->json([
            'data' => $branch
        ], 201);
    }

    public function update($id){

        $data = $this->request->all();

        $branch = Branch::whereId($id);
        
        $branch->update([
            'name' => $data['name'],
            'GSID' => $data['GSID'],
            // 'org_id' => $data['org_id'],
            'address' => $data['address'], 
            'state_id' => $data['state_id'], 
            'city_name' => $data['city_name'], 
            'postal_code' => $data['postal_code'], 

        ]);

        $file = $this->request->file('logo');
        if($file){
            $fileName = auth()->id() . '_' . time() . '_'. $file->getClientOriginalName();  
            $file->move(public_path('images/logos'), $fileName);
    
            $branch->update([
                'logo_path' => url('images/logos') . "/" . $fileName,
            ]);
        }

        return response()->json([
            'data' => $branch
        ], 201);
    }

    public function delete($id){
        $branch = Branch::whereId($id);
        $branch->delete();

        return response()->json([
            'data' => "Branch was deleted!"
        ]);
    }

    public function request($request)
    {
       $this->request = $request;
       return $this;
    }
}