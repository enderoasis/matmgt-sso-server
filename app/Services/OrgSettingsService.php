<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Organization;
use App\Models\OrgSettings;
use Validator;

class OrgSettingsService 
{

    public function one($id){

        $org_settings = OrgSettings::where('org_id',$id)->first();

        if(!$org_settings){
            $org_settings = OrgSettings::setModel($id);
        }
        
        return response()->json([
            'data' => $org_settings
        ]);

    }


    public function update($id){

       $org_settings = OrgSettings::where('org_id',$id)->first();
       $data = $this->request->all();
        
        $org_settings->update([
            'acc_software_id' => $data['acc_software_id'],
            'acc_system_connection_string' => $data['acc_system_connection_string'], 
            'agent_version' => $data['agent_version'], 
            'acc_software_version' => $data['acc_software_version'], 
            'agent_autoupdate' => $data['agent_autoupdate'], 

        ]);

        return response()->json([
            'data' => $org_settings
        ]);

    }

    public function request($request)
    {
       $this->request = $request;
       return $this;
    }
}