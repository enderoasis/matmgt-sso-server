<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Organization;
use App\Models\Branch;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Validator;

class OrgService 
{

    public function all(){

        $orgs = Organization::leftJoin('states', 'states.id', '=', 'organizations.state_id')
                ->leftJoin('users', 'users.id', '=', 'organizations.created_by')
                ->select('organizations.id as id',
                    'organizations.name as name',
                    'organizations.GSID as GSID',
                    'organizations.logo_path as logo_path',
                    'organizations.address as address',
                    'organizations.state_id as state_id',
                    'organizations.city_name as city_name',
                    'users.name as created_by',
                    'states.name as state_name',
                    'organizations.postal_code as postal_code',
                    'organizations.notes as notes',
                    'organizations.created_at as created_at',
                    'organizations.updated_at as updated_at'
                )
                ->orderBy('created_at','DESC')
                ->get();

        return response()->json([
            'data' =>  $orgs
        ]);
    }

    public function create(){

        $data = $this->request->all();
        $validator = Validator::make($data, [
            'GSID' => 'required|integer|unique:organizations|unique:branches'
        ]);
        
        if ($validator->fails()) {
            return response()->json(['success' => false, 'response' => $validator->errors()], 400);
        }

        $organization = Organization::create([
            'name' => $data['name'],
            'GSID' => $data['GSID'],
            'address' => $data['address'],
            'state_id' => $data['state_id'],
            'city_name' => $data['city_name'],
            'postal_code' => $data['postal_code'],
            'created_by' => auth()->user()->id
        ]);

        // Create branch, passing organization id in it
        if($organization){
            $organization_branch_main = Branch::create([
                'name' => $data['name'],
                'org_id' => $organization['id'],
                'GSID' => $data['GSID'],
                'address' => $data['address'],
                'state_id' => $data['state_id'],
                'city_name' => $data['city_name'],
                'postal_code' => $data['postal_code'],
                'created_by' => auth()->user()->id
            ]);
            // Upload file if present
            
            $file = $this->request->file('logo');
            if($file){
                $fileName = auth()->id() . '_' . time() . '_'. $file->getClientOriginalName();  
                $file->move(public_path('images/logos'), $fileName);
        
                $organization->update([
                    'logo_path' => url('images/logos') . "/" . $fileName,
                ]);

                $organization_branch_main->update([
                    'logo_path' => url('images/logos') . "/" . $fileName,
                ]);
            }
        }

        return response()->json([
            'data' => $organization
        ]);
    }

    public function update($id){

        $data = $this->request->all();

        $organization = Organization::whereId($id);
        
        $organization->update([
            'name' => $data['name'],
            'GSID' => $data['GSID'],
            'address' => $data['address'], 
            'state_id' => $data['state_id'], 
            'city_name' => $data['city_name'], 
            'postal_code' => $data['postal_code'], 
        ]);

        $file = $this->request->file('logo');
        
        if($file){
            $fileName = auth()->id() . '_' . time() . '_'. $file->getClientOriginalName();  
            $file->move(public_path('images/logos'), $fileName);
    
            $organization->update([
                'logo_path' => url('images/logos') . "/" . $fileName,
            ]);

        }

        return response()->json([
            'data' => $organization
        ]);
    }

    public function delete($id){
        $organization = Organization::whereId($id);
        $organization->delete();

        return response()->json([
            'data' => "Organization was deleted!"
        ]);
    }

    public function request($request)
    {
       $this->request = $request;
       return $this;
    }
}