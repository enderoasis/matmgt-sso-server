<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\User;
use App\Mail\UserResgisteredMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Validator;

class UserService 
{

    public function all(){

        $users = User::all();

        return response()->json([
            'data' =>  $users
        ]);
    }

    public function create(){

        $data = $this->request->all();

        $validator = Validator::make($data, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|string|email:rfc,dns|max:50|unique:users',
            'phone' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'response' => $validator->errors()], 400);
        }

        $tmp_password = Str::random(5);

        $response = User::create([
            'name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($tmp_password)
        ]);

        \Mail::to($response['email'])->queue(new UserResgisteredMail($response,$tmp_password));

        return response()->json(['success' => true, 'response' => $response], 201);
    }

    public function update($id){

        $data = $this->request->all();

        $user = User::whereId($id);
        $user->update([
            'name' => $data['name'],
            'last_name' => $data['last_name'],
            'phone' => $data['phone'], 
            'email' => $data['email']
        ]);

        return response()->json(['success' => true, 'response' => $user], 201);
    }

    public function one($id){

        $user = User::findOrFail($id);

        return response()->json(['success' => true, 'data' => $user], 200);
    }

    public function avatarUpload(){

        $data = $this->request->all();

        $user = User::whereId($data['user_id']);

        $file = $this->request->file('avatar');
        $fileName = auth()->id() . '_' . time() . '.'. $file->extension();  
        $file->move(public_path('images/avatars'), $fileName);

        $user->update([
            'avatar' => url('images/avatars') . "/" . $fileName,
        ]);

        return response()->json(['success' => true, 'data' => $user], 201);

    }
    public function delete($id){

    }

    public function request($request)
    {
       $this->request = $request;
       return $this;
    }
}