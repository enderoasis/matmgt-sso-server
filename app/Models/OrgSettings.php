<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrgSettings extends Model
{
    use HasFactory;

    protected $fillable = ['acc_software_id','org_id','acc_system_connection_string','agent_version','acc_software_version','agent_autoupdate'];

    public static function setModel($org_id){
        $model = new self();
        $model->acc_software_id = null;
        $model->org_id = $org_id;
        $model->acc_system_connection_string = null;
        $model->agent_version = null;
        $model->acc_software_version = null;
        $model->save();

        return $model;
    }
}
