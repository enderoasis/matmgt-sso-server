<?php

namespace App\Interfaces;

interface OrganizationRepositoryInterface 
{
    public function getAllOrganizations();
    public function getOrganizationById($organization_id);
    public function deleteOrganization($organization_id);
    public function createOrganization(array $details);
    public function updateOrganization($organization_id, array $details);
}