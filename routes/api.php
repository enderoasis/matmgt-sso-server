<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\API\CityController;
use App\Http\Controllers\API\StateController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login',[LoginController::class,'loginApi']);
Route::get('cities/state/{id}',[CityController::class,'getCitiesByStateID']);
Route::get('states',[StateController::class,'index']);

Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'v1',
    'namespace' => 'App\Http\Controllers\API'
], function ($router) {
    Route::resource('users', 'UserController');
    Route::post('users/avatar','UserController@avatarUpload');
    Route::resource('orgs', 'OrganizationController');
    Route::resource('softwares', 'AccSoftwareController');
    Route::resource('organization/settings', 'OrgSettingsController');
    Route::resource('branches/organization', 'BranchController');

});