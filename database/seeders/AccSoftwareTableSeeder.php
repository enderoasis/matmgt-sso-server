<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AccSoftware;

class AccSoftwareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AccSoftware::insert([
            [
                'name'   => 'Spectrum'
            ], 
            [
                'name'   => 'eCMS'
            ], 
            [
                'name'   => 'Vista'
            ],
            [
                'name'   => 'PoserPurchase'
            ],
            [
                'name'   => 'TimberlinePurchaseOrder'
            ],
            [
                'name'   => 'TimberlineAccounting'
            ],
            [
                'name'   => 'QuickBooks'
            ],
            [
                'name'   => 'ViewPoint'
            ],
            [
                'name'   => 'Jonas'
            ],
            [
                'name'   => 'ComputerEase'
            ]
        ]);
    }
}
