<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = 'Didar';
        $user->last_name = 'Temirkhanov';
        $user->email = 'dtemirkhanov@matmgt.com';
        $user->password = bcrypt('545123dd');
        $user->save();
    }
}
