<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Organization;

class OrgsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        // Customer organization fake data
        foreach(range(1,40) as $index){
            \DB::table('organizations')->insert([
                'name' => $faker->company,
                'GSID' => $faker->unique()->ean8,
                'logo_path' => $faker->imageUrl($width = 340, $height = 180),
                'address' => $faker->streetAddress,
                'state_id' => rand(1,50),
                'postal_code' => $faker->postcode
            ]);
        }
    }
}
