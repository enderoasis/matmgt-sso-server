<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\State;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = array(
            array("id"=>"1","code"=>"AL","name"=>"Alabama"),
            array("id"=>"2","code"=>"AK","name"=>"Alaska"),
            array("id"=>"3","code"=>"AZ","name"=>"Arizona"),
            array("id"=>"4","code"=>"AR","name"=>"Arkansas"),
            array("id"=>"5","code"=>"CA","name"=>"California"),
            array("id"=>"6","code"=>"CO","name"=>"Colorado"),
            array("id"=>"7","code"=>"CT","name"=>"Connecticut"),
            array("id"=>"8","code"=>"DE","name"=>"Delaware"),
            array("id"=>"9","code"=>"DC","name"=>"District of Columbia"),
            array("id"=>"10","code"=>"FL","name"=>"Florida"),
            array("id"=>"11","code"=>"GA","name"=>"Georgia"),
            array("id"=>"12","code"=>"HI","name"=>"Hawaii"),
            array("id"=>"13","code"=>"ID","name"=>"Idaho"),
            array("id"=>"14","code"=>"IL","name"=>"Illinois"),
            array("id"=>"15","code"=>"IN","name"=>"Indiana"),
            array("id"=>"16","code"=>"IA","name"=>"Iowa"),
            array("id"=>"17","code"=>"KS","name"=>"Kansas"),
            array("id"=>"18","code"=>"KY","name"=>"Kentucky"),
            array("id"=>"19","code"=>"LA","name"=>"Louisiana"),
            array("id"=>"20","code"=>"ME","name"=>"Maine"),
            array("id"=>"21","code"=>"MD","name"=>"Maryland"),
            array("id"=>"22","code"=>"MA","name"=>"Massachusetts"),
            array("id"=>"23","code"=>"MI","name"=>"Michigan"),
            array("id"=>"24","code"=>"MN","name"=>"Minnesota"),
            array("id"=>"25","code"=>"MS","name"=>"Mississippi"),
            array("id"=>"26","code"=>"MO","name"=>"Missouri"),
            array("id"=>"27","code"=>"MT","name"=>"Montana"),
            array("id"=>"28","code"=>"NE","name"=>"Nebraska"),
            array("id"=>"29","code"=>"NV","name"=>"Nevada"),
            array("id"=>"30","code"=>"NH","name"=>"New Hampshire"),
            array("id"=>"31","code"=>"NJ","name"=>"New Jersey"),
            array("id"=>"32","code"=>"NM","name"=>"New Mexico"),
            array("id"=>"33","code"=>"NY","name"=>"New York"),
            array("id"=>"34","code"=>"NC","name"=>"North Carolina"),
            array("id"=>"35","code"=>"ND","name"=>"North Dakota"),
            array("id"=>"36","code"=>"OH","name"=>"Ohio"),
            array("id"=>"37","code"=>"OK","name"=>"Oklahoma"),
            array("id"=>"38","code"=>"OR","name"=>"Oregon"),
            array("id"=>"39","code"=>"PA","name"=>"Pennsylvania"),
            array("id"=>"40","code"=>"PR","name"=>"Puerto Rico"),
            array("id"=>"41","code"=>"RI","name"=>"Rhode Island"),
            array("id"=>"42","code"=>"SC","name"=>"South Carolina"),
            array("id"=>"43","code"=>"SD","name"=>"South Dakota"),
            array("id"=>"44","code"=>"TN","name"=>"Tennessee"),
            array("id"=>"45","code"=>"TX","name"=>"Texas"),
            array("id"=>"46","code"=>"UT","name"=>"Utah"),
            array("id"=>"47","code"=>"VT","name"=>"Vermont"),
            array("id"=>"48","code"=>"VA","name"=>"Virginia"),
            array("id"=>"49","code"=>"WA","name"=>"Washington"),
            array("id"=>"50","code"=>"WV","name"=>"West Virginia"),
            array("id"=>"51","code"=>"WI","name"=>"Wisconsin"),
            array("id"=>"52","code"=>"WY","name"=>"Wyoming")
        );
    
        foreach($states as $state){
            State::create($state);
        }
    }
}
