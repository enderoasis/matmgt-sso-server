<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('GSID')->unique();
            $table->integer('org_id')->unsigned()->nullable();
            $table->string('logo_path')->nullable();
            $table->string('address')->nullable();
            $table->integer('state_id');
            $table->string('city_name')->nullable();
            $table->string('postal_code')->nullable();
            $table->text('notes')->nullable();
            $table->integer('created_by')->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
