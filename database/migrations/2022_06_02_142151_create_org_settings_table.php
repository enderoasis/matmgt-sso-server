<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrgSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('org_settings', function (Blueprint $table) {
            $table->id();
            $table->integer('acc_software_id')->unsigned()->nullable();
            // Organization ID
            $table->integer('org_id')->unsigned()->nullable();
            // DB Connection string
            $table->text('acc_system_connection_string')->nullable();
            // Local Agent version
            $table->string('agent_version')->nullable();
            // Accounting system version
            $table->string('acc_software_version')->nullable();
            $table->boolean('agent_autoupdate')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('org_settings');
    }
}
